# Firebase authentication without Play Services

Early beta, very unstable.
Allows you to run Firebase Auth on devices without Play Services by replacing ComponentRegistrar in ComponentDiscoveryService.
WARNING: this library is incompatible with `com.google.firebase:firebase-auth`

## Setup

To test this example you have to set up your own firebase project in firebase console. Don't forget to add google-services.json at the /app folder.
Add to app level build.gradle:
```groovy
android {
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}
```

## Usage

```java
public class MyActivity extends AppCompatActivity implements OnAuthStateChangeListener /* optional implementation */ {
    /*some code...*/

    private void obtainAuthInstance(){ 
         FirebaseApp app = FirebaseApp.getInstance();
         // This will get saved user state from storage and then update it with refresh token. 
         FirebaseRestAuth auth = FirebaseRestAuth.getInstance(app);
         // This will set listener to user auth state and refresh token once it is close to 
         // expiration time. You need to set this to have up-to-date information about user.
         auth.getTokenRefresher().bindTo(this);
         // Optional: will bind listener to this activity so it will receive updates about user
         // state. Warning: once you call this, it will immediately return state, so you should
         // probably set it after you initialized all views.
         auth.getTokenRefresher().addStateListener(this);
    }

    private void SignUpWithEmail(String email, String password) {
        auth.signUpWithEmail(email, password)
        .addOnSuccessListener((res)->{/* update UI */});
    }

     private void signOut() {
        auth.signOut();
        /* update UI */
    }

    @Override
    public void onStateChanged(@Nullable FirebaseRestAuthUser user) {
        if (user!=null)
            onUpdateReceived("Signed in as " + user);
        else onUpdateReceived("Signed out");
    }
}
```

## Dependency

```groovy
implementation 'io.gitlab.liquid-soap:firebase-auth-wo-play-services:0.0.2-SNAPSHOT'
```