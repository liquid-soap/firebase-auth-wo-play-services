/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.example.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.FirebaseFirestore;

import io.gitlab.liquid_soap.example.databinding.FragmentAnonBinding;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuthUser;

public class FragmentAnon extends BaseFragment {
    private FragmentAnonBinding b;

    public FragmentAnon() {
        /* required */
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        b = FragmentAnonBinding.inflate(getLayoutInflater(), container, false);
        super.onCreateView(inflater, container, savedInstanceState);
        return b.getRoot();
    }

    @Override
    void setOnClickListeners() {
        b.logInAnon.setOnClickListener(this::signUpAnon);
        b.logOutAnon.setOnClickListener(this::signOut);
        b.getDocAnon.setOnClickListener(this::getDocument);
    }

    private void getDocument(View view) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.document("documents/doc").get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null)
                b.getDocAnonResult.setText("Successful get: " + task.getResult().getData());
            else {
                Exception e = task.getException();
                if (e != null) {
                    errorReceived(e);
                    Log.e("getDocument", e.getMessage(), e);
                    b.getDocAnonResult.setText("Unable to get document due to error");
                } else
                    b.getDocAnonResult.setText("Unable to get document. Did you create it at documents/doc?");
            }
        });
    }

    private void signUpAnon(View view) {
        auth.signInAnonymously()
                .addOnSuccessListener(res -> {
                    Log.d(TAG, "signInAnonymously: " + res);
                    updateUI();
                    errorCleared();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "signInAnonymously: failure", e);
                    errorReceived(e);
                    updateUI();
                });
    }


    private void signOut(View view) {
        auth.signOut();
        errorCleared();
        updateUI();
    }

    @Override
    public void updateUI() {
        FirebaseRestAuthUser user = auth.getCurrentUser();
        boolean signedIn = user != null;

        b.logInAnon.setEnabled(!signedIn);
        b.logOutAnon.setEnabled(signedIn);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        b = null;
    }
}
