/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.example.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.google.android.material.tabs.TabLayoutMediator;

import io.gitlab.liquid_soap.example.R;
import io.gitlab.liquidsoap.example.fragments.BaseFragment;
import io.gitlab.liquidsoap.example.fragments.FragmentAnon;
import io.gitlab.liquidsoap.example.fragments.FragmentEmail;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuth;

public class SectionsPagerAdapter extends FragmentStateAdapter {
    @StringRes
    private final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2};
    private final BaseFragment[] fragments = new BaseFragment[]{new FragmentAnon(), new FragmentEmail()};
    public final TabLayoutMediator.TabConfigurationStrategy strategy = (tab, position) -> {
        tab.setCustomView(R.layout.custom_tab);
        tab.setText(TAB_TITLES[position]);
    };

    public SectionsPagerAdapter(FragmentActivity activity, FirebaseRestAuth auth, FragmentManager fm) {
        super(activity);
        for (BaseFragment f : fragments) f.setAuth(auth);
    }

    @NonNull
    @Override
    public BaseFragment createFragment(int position) {
        return fragments[position];
    }


    @Override
    public int getItemCount() {
        return fragments.length;
    }
}