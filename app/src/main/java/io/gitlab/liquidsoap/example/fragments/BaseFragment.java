/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.example.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import io.gitlab.liquidsoap.example.intefaces.FragmentCommunicationInterface;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuth;

public abstract class BaseFragment extends Fragment {
    final String TAG = getClass().getSimpleName();
    FirebaseRestAuth auth;

    public BaseFragment() {
        /*required*/
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setOnClickListeners();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    abstract void setOnClickListeners();

    public void setAuth(FirebaseRestAuth auth) {
        this.auth = auth;
    }

    public void errorCleared() {
        if (getActivity() instanceof FragmentCommunicationInterface) {
            ((FragmentCommunicationInterface) getActivity()).onErrorCleared();
        }
    }

    public void errorReceived(Exception e) {
        if (getActivity() instanceof FragmentCommunicationInterface) {
            ((FragmentCommunicationInterface) getActivity()).onErrorReceived(e.getMessage() == null ? e.toString() : e.getMessage());
        }
    }

    public abstract void updateUI();

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }
}
