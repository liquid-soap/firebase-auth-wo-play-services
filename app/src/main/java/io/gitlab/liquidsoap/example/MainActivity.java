/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.example;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.FirebaseApp;

import io.gitlab.liquid_soap.example.R;
import io.gitlab.liquid_soap.example.databinding.ActivityMainBinding;
import io.gitlab.liquidsoap.example.adapters.SectionsPagerAdapter;
import io.gitlab.liquidsoap.example.intefaces.FragmentCommunicationInterface;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuth;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuthUser;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.OnAuthStateChangeListener;

public class MainActivity extends AppCompatActivity implements FragmentCommunicationInterface, OnAuthStateChangeListener {
    private ActivityMainBinding binding;
    SectionsPagerAdapter sectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        FirebaseApp app = FirebaseApp.getInstance();
        // This will get saved user state from storage and then update it.
        FirebaseRestAuth auth = FirebaseRestAuth.getInstance(app);

        sectionsPagerAdapter = new SectionsPagerAdapter(this, auth, getSupportFragmentManager());
        binding.viewPager.setAdapter(sectionsPagerAdapter);
        binding.viewPager.setOffscreenPageLimit(2);

        new TabLayoutMediator(binding.chooseLoginMethod,
                binding.viewPager,
                sectionsPagerAdapter.strategy).attach();


        // This will set listener to user auth state and refresh token once it is close to
        // expiration time. You need to set this to have up-to-date information about user.
        auth.getTokenRefresher().bindTo(this).addStateListener(this);
    }

    @Override
    public void onErrorReceived(String error) {
        binding.error.setVisibility(View.VISIBLE);
        binding.error.setText(error);
    }

    @Override
    public void onErrorCleared() {
        binding.error.setVisibility(View.GONE);
    }

    @Override
    public void onStateChanged(@Nullable FirebaseRestAuthUser user) {
        if (user == null)
            binding.authStatus.setText(("Signed out"));
        else
            binding.authStatus.setText(("Signed in as " + user.getUserId()));
    }
}