/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.example.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.FirebaseFirestore;

import io.gitlab.liquid_soap.example.databinding.FragmentEmailBinding;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuthUser;

public class FragmentEmail extends BaseFragment {
    private FragmentEmailBinding b;

    public FragmentEmail() {
        /* required */
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        b = FragmentEmailBinding.inflate(getLayoutInflater(), container, false);
        super.onCreateView(inflater, container, savedInstanceState);
        return b.getRoot();
    }

    @Override
    void setOnClickListeners() {
        b.signUpEmail.setOnClickListener(this::signUpWithEmail);
        b.logInEmail.setOnClickListener(this::logInWithEmail);
        b.logOutEmail.setOnClickListener(this::signOut);
        b.getDocEmail.setOnClickListener(this::getDocumentFromFirestore);
    }

    private void signUpWithEmail(View view) {
        String password = checkPassword();
        String email = checkEmail();

        if (email != null && password != null)
            auth.signUpWithEmail(email, password)
                    .addOnSuccessListener(res -> {
                        Log.d(TAG, "signUpWithEmail: " + res);
                        errorCleared();
                        updateUI();
                    })
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "signUpWithEmail: failure", e);
                        errorReceived(e);
                        updateUI();
                    });
    }

    private void logInWithEmail(View view) {
        String password = checkPassword();
        String email = checkEmail();

        if (email != null && password != null)
            auth.signInWithEmail(email, password)
                    .addOnSuccessListener(res -> {
                        Log.d(TAG, "signInWithEmail: " + res);
                        errorCleared();
                        updateUI();
                    })
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "signInWithEmail: failure", e);
                        errorReceived(e);
                        updateUI();
                    });
    }

    private void signOut(View view) {
        auth.signOut();
        errorCleared();
        updateUI();
    }

    private void getDocumentFromFirestore(View view) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.document("documents/doc").get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null)
                b.getDocEmailResult.setText("Successful get: " + task.getResult().getData());
            else {
                Exception e = task.getException();
                if (e != null) {
                    errorReceived(e);
                    Log.e("getDocument", e.getMessage(), e);
                    b.getDocEmailResult.setText("Unable to get document due to error");
                } else
                    b.getDocEmailResult.setText("Unable to get document. Did you create it at documents/doc?");
            }
        });
    }

    private String checkEmail() {
        String email = null;
        if (b.emailInputText.getText() != null) {
            email = b.emailInputText.getText().toString().trim();
            if ("".equals(email)) email = null;
        }
        if (email == null)
            b.emailInputText.setError("Required");

        return email;
    }

    private String checkPassword() {
        String password = null;
        if (b.passwordInputText.getText() != null) {
            password = b.passwordInputText.getText().toString().trim();
            if ("".equals(password)) password = null;
        }
        if (password == null)
            b.passwordInputText.setError("Required");

        return password;
    }

    @Override
    public void updateUI() {
        FirebaseRestAuthUser user = auth.getCurrentUser();
        boolean signedIn = user != null;

        b.signUpEmail.setEnabled(!signedIn);
        b.logInEmail.setEnabled(!signedIn);
        b.logOutEmail.setEnabled(signedIn);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        b = null;
    }
}