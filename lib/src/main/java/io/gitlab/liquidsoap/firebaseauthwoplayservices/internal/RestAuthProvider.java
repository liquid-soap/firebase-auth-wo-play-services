/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.internal;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.internal.IdTokenListener;
import com.google.firebase.internal.InternalTokenResult;
import com.google.firebase.internal.api.FirebaseNoSignedInUserException;

import java.util.ArrayList;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuth;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuthUser;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseTokenRefresher;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.service.FirebaseKeyInterceptor;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.service.IdentityToolkitApi;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.service.SecureTokenApi;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.FetchProvidersForEmailRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.FetchProvidersForEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.OOBEmailRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.OOBEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInAnonymouslyRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInAnonymouslyResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithCustomTokenRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithCustomTokenResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithOAuthRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithOAuthResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignUpWithEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignWithEmailRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.VerifyOOBRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.VerifyOOBResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.securetoken.ExchangeTokenRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.securetoken.ExchangeTokenResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.utils.ExpirationUtils;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.utils.IdTokenParser;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.utils.RetrofitUtils;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.utils.UserStorage;
import okhttp3.OkHttpClient;
import retrofit2.Call;

import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.INVALID_REFRESH_TOKEN;

/**
 * RestAuthProvider <- FirebaseRestAuth <- InternalAuthProvider <- InternalTokenProvider
 */
public final class RestAuthProvider implements FirebaseRestAuth {
    private static final String TAG = "RestAuthProvider";
    private final UserStorage userStorage;
    private final ArrayList<IdTokenListener> listeners = new ArrayList<>();
    private final IdentityToolkitApi firebaseApi;
    private final SecureTokenApi secureTokenApi;
    private final FirebaseTokenRefresher tokenRefresher;
    private FirebaseRestAuthUser currentUser = null;

    /**
     * Implementation of FirebaseRestAuth
     *
     * @param app FirebaseApp
     */
    public RestAuthProvider(@NonNull FirebaseApp app) {
        Context context = app.getApplicationContext();
        String apiKey = app.getOptions().getApiKey();
        userStorage = new UserStorage(context, app);
        tokenRefresher = new FirebaseTokenRefresher(this);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new FirebaseKeyInterceptor(apiKey))
                .build();

        firebaseApi = IdentityToolkitApi.getInstance(client);
        secureTokenApi = SecureTokenApi.getInstance(client);

        FirebaseRestAuthUser user = userStorage.get();
        setCurrentUser(user);
        getAccessToken(true);
    }

    @NonNull
    @Override
    public FirebaseTokenRefresher getTokenRefresher() {
        return this.tokenRefresher;
    }

    @Nullable
    @Override
    public FirebaseRestAuthUser getCurrentUser() {
        return this.currentUser;
    }


    public void setCurrentUser(@Nullable FirebaseRestAuthUser user) {
        Log.d(TAG, "currentUser = " + user);
        currentUser = user;
        userStorage.set(user);
        for (IdTokenListener listener : listeners) {
            listener.onIdTokenChanged(
                    new InternalTokenResult(user != null ? user.getIdToken() : null)
            );
        }
    }

    @Override
    public Task<SignInAnonymouslyResponse> signInAnonymously() {
        return RetrofitUtils.callToTask(
                firebaseApi.signInAnonymously(new SignInAnonymouslyRequest())
        ).addOnSuccessListener(res ->
                setCurrentUser(new FirebaseRestAuthUser(res.idToken, null, res.refreshToken,
                        res.localId))
        ).addOnFailureListener(e -> setCurrentUser(null));
    }

    @Override
    public Task<SignInWithCustomTokenResponse> signInWithCustomToken(@NonNull String token) {
        return RetrofitUtils.callToTask(
                firebaseApi.signInWithCustomToken(new SignInWithCustomTokenRequest(token))
        ).addOnSuccessListener(res ->
                setCurrentUser(new FirebaseRestAuthUser(res.idToken, res.refreshToken))
        ).addOnFailureListener(e -> setCurrentUser(null));
    }


    @Override
    public Task<SignInWithEmailResponse> signInWithEmail(@NonNull String email,
                                                         @NonNull String password) {
        return RetrofitUtils.callToTask(
                this.firebaseApi.signInWithPassword(new SignWithEmailRequest(email, password))
        ).addOnSuccessListener(res ->
                setCurrentUser(new FirebaseRestAuthUser(res.idToken, res.email, res.refreshToken,
                        res.refreshToken))
        ).addOnFailureListener(e -> setCurrentUser(null));
    }

    @Override
    public Task<SignUpWithEmailResponse> signUpWithEmail(@NonNull String email,
                                                         @NonNull String password) {
        return RetrofitUtils.callToTask(firebaseApi.signUpWithEmail(
                new SignWithEmailRequest(email, password)
        )).addOnSuccessListener(res ->
                setCurrentUser(new FirebaseRestAuthUser(res.idToken, res.email,
                        res.refreshToken, res.localId))
        ).addOnFailureListener(e -> setCurrentUser(null));
    }

    @Override
    public Task<SignInWithOAuthResponse> signInWithOAuth(@NonNull String requestUri,
                                                         @NonNull String postBody,
                                                         boolean returnIdpCredential) {
        return RetrofitUtils.callToTask(firebaseApi.signInWithOAuth(
                new SignInWithOAuthRequest(requestUri, postBody, returnIdpCredential)
        )).addOnSuccessListener(res ->
                setCurrentUser(new FirebaseRestAuthUser(res.idToken, res.email, res.refreshToken,
                        res.localId))
        ).addOnFailureListener(e -> setCurrentUser(null));
    }

    @Override
    public Task<FetchProvidersForEmailResponse> fetchEmailProviders(@NonNull String identifier,
                                                                    @NonNull String continueUri) {
        return RetrofitUtils.callToTask(firebaseApi.fetchEmailProviders(
                new FetchProvidersForEmailRequest(identifier, continueUri)
        ));
    }

    @Override
    public Task<OOBEmailResponse> sendOOBEmailCode(@NonNull String requestType,
                                                   @NonNull String email) {
        return RetrofitUtils.callToTask(firebaseApi.sendOOBEmailCode(
                new OOBEmailRequest(requestType, email)
        ));
    }

    @Override
    public Task<VerifyOOBResponse> verifyOOBEmailCode(@NonNull String oobCode) {
        return RetrofitUtils.callToTask(firebaseApi.verifyOOBEmailCode(
                new VerifyOOBRequest(oobCode)
        ));
    }

    @Override
    public void signOut() {
        setCurrentUser(null);
    }

    private Task<ExchangeTokenResponse> refreshUserToken() throws RefreshTokenException {
        String refreshToken = currentUser == null ? null : currentUser.getRefreshToken();
        if (refreshToken == null)
            throw new RefreshTokenException(INVALID_REFRESH_TOKEN);

        ExchangeTokenRequest request =
                new ExchangeTokenRequest("refresh_token", refreshToken);
        Call<ExchangeTokenResponse> call = secureTokenApi.exchangeToken(request);

        return RetrofitUtils.callToTask(call)
                .addOnSuccessListener(res ->
                        setCurrentUser(new FirebaseRestAuthUser(res.id_token, null,
                                res.refresh_token, res.user_id)))
                .addOnFailureListener(e -> {
                    FirebaseException exception;
                    if (e instanceof RefreshTokenException) {
                        exception = new RefreshTokenException(((RefreshTokenException) e).getResponseErrorMessage(), e)
                                .setResponseErrorCode(((RefreshTokenException) e).getResponseErrorCode())
                                .setResponseErrorStatus(((RefreshTokenException) e).getResponseErrorStatus());
                    } else exception = new FirebaseException("Failed to refresh token", e);
                    logErrorProperly(exception);
                });
    }

    private GetTokenResult currentUserToTokenResult() {
        FirebaseRestAuthUser user = this.getCurrentUser();
        if (user != null) {
            String token = user.getIdToken();
            return new GetTokenResult(token, IdTokenParser.parseIdToken(token));
        } else return null;
    }

    @NonNull
    @Override
    public Task<GetTokenResult> getAccessToken(boolean forceRefresh) {
        final TaskCompletionSource<GetTokenResult> source = new TaskCompletionSource<>();
        FirebaseRestAuthUser user = this.getCurrentUser();
        if (user != null) {
            boolean needsRefresh = forceRefresh || ExpirationUtils.isExpired(user);
            if (!needsRefresh) {
                // Return the current token, no need to check anything
                source.trySetResult(currentUserToTokenResult());
            } else {
                // Get a new token and then return
                try {
                    this.refreshUserToken()
                            .addOnSuccessListener(exchangeTokenResponse ->
                                    source.trySetResult(currentUserToTokenResult()))
                            .addOnFailureListener(e -> {
                                FirebaseException exception;
                                if (e instanceof RefreshTokenException) {
                                    exception = new RefreshTokenException(((RefreshTokenException) e).getResponseErrorMessage(), e)
                                            .setResponseErrorCode(((RefreshTokenException) e).getResponseErrorCode())
                                            .setResponseErrorStatus(((RefreshTokenException) e).getResponseErrorStatus());
                                } else
                                    exception = new FirebaseException("Failed to refresh token", e);
                                logErrorProperly(exception);
                                source.trySetException(e);
                            });
                } catch (RefreshTokenException e) {
                    logErrorProperly(e);
                }
            }
        } else {
            // Not yet signed in
            source.trySetException(new FirebaseNoSignedInUserException("Please sign in " +
                    "before trying to get a token"));
        }

        return source.getTask();
    }

    private void logErrorProperly(FirebaseException e) {
        if (e instanceof RefreshTokenException) {
            CommonErrorMessages errorMessage = ((RefreshTokenException) e).getResponseErrorMessage();
            if (CommonErrorMessages.TOKEN_EXPIRED.equals(errorMessage) ||
                    CommonErrorMessages.USER_DISABLED.equals(errorMessage) ||
                    CommonErrorMessages.USER_NOT_FOUND.equals(errorMessage))
                signOut();

            int errorCode = ((RefreshTokenException) e).getResponseErrorCode();
            String errorStatus = ((RefreshTokenException) e).getResponseErrorStatus();
            Log.e(TAG, "(" + errorCode + ") RefreshTokenException:" + e.getMessage() +
                            " with errorStatus " + errorStatus,
                    e);
        } else
            Log.e(TAG, "RefreshTokenException:" + e.getMessage(), e);
    }

    @Nullable
    @Override
    public String getUid() {
        if (currentUser != null)
            return currentUser.getUserId();
        else return null;
    }

    @Override
    public void addIdTokenListener(@NonNull IdTokenListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeIdTokenListener(@NonNull IdTokenListener listener) {
        listeners.remove(listener);
    }
}
