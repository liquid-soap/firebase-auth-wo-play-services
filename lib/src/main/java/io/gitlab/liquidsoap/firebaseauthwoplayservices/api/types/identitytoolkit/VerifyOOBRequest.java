/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit;

import androidx.annotation.NonNull;

/**
 * Reference:
 * https://firebase.google.com/docs/reference/rest/auth#section-verify-password-reset-code
 */
public class VerifyOOBRequest {
    /**
     * The email action code sent to the user's email for resetting the password.
     */
    public String oobCode;

    public VerifyOOBRequest(String oobCode) {
        this.oobCode = oobCode;
    }

    @Override
    @NonNull
    public String toString() {
        return "VerifyOOBRequest{" +
                "oobCode='" + oobCode + '\'' +
                '}';
    }
}
