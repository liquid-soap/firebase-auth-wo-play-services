/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.securetoken;


import androidx.annotation.NonNull;

/**
 * Reference: https://firebase.google.com/docs/reference/rest/auth#section-refresh-token
 */
public class ExchangeTokenRequest {
    /**
     * The refresh token's grant type, always "refresh_token".
     */
    public final String grant_type;
    /**
     * A Firebase Auth refresh token.
     */
    public final String refresh_token;

    public ExchangeTokenRequest(String grant_type, String refresh_token) {
        this.grant_type = grant_type;
        this.refresh_token = refresh_token;
    }

    @NonNull
    @Override
    public String toString() {
        return "ExchangeTokenRequest{" +
                "grant_type='" + grant_type + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                '}';
    }

}
