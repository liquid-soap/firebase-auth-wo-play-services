/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.internal.InternalAuthProvider;

import java.util.HashMap;
import java.util.Map;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.FetchProvidersForEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.OOBEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInAnonymouslyResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithCustomTokenResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithOAuthResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignUpWithEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.VerifyOOBResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.internal.RestAuthProvider;

public interface FirebaseRestAuth extends InternalAuthProvider {

    @NonNull
    FirebaseTokenRefresher getTokenRefresher();

    @Nullable
    FirebaseRestAuthUser getCurrentUser();

    void setCurrentUser(@Nullable FirebaseRestAuthUser user);

    Task<SignInAnonymouslyResponse> signInAnonymously();

    Task<SignInWithCustomTokenResponse> signInWithCustomToken(@NonNull String token);

    Task<SignInWithEmailResponse> signInWithEmail(@NonNull String email, @NonNull String password);

    Task<SignUpWithEmailResponse> signUpWithEmail(@NonNull String email, @NonNull String password);

    Task<SignInWithOAuthResponse> signInWithOAuth(@NonNull String requestUri,
                                                  @NonNull String postBody,
                                                  boolean returnIdpCredential);

    Task<FetchProvidersForEmailResponse> fetchEmailProviders(@NonNull String identifier,
                                                             @NonNull String continueUri);

    Task<OOBEmailResponse> sendOOBEmailCode(@NonNull String requestType, @NonNull String email);

    Task<VerifyOOBResponse> verifyOOBEmailCode(@NonNull String oobCode);

    void signOut();

    static FirebaseRestAuth getInstance(FirebaseApp app) {
        // only need one RestAuthProvider for App
        if (!HiddenClass.INSTANCE.containsKey(app.getName())) {
            RestAuthProvider instance = new RestAuthProvider(app);
            HiddenClass.INSTANCE.put(app.getName(), instance);
        }

        return HiddenClass.INSTANCE.get(app.getName());
    }

    final class HiddenClass {
        private static final Map<String, RestAuthProvider> INSTANCE = new HashMap<>();

        private HiddenClass() {
        }
    }
}
