/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Map;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.utils.IdTokenParser;

//todo: find out whenever OAuth users localId and Firebase uid differ
public class FirebaseRestAuthUser {
    private final String idToken;
    @Nullable
    private final String email;
    private final String refreshToken;

    private String userId;
    private final long expirationTime;


    public FirebaseRestAuthUser(String idToken, String refreshToken) {
        this(idToken, null, refreshToken, null);
    }

    public FirebaseRestAuthUser(String idToken, @Nullable String email, String refreshToken,
                                @Nullable String localId) {
        this.idToken = idToken;
        this.email = email;
        this.refreshToken = refreshToken;
        this.userId = localId;

        Map<String, Object> claims = IdTokenParser.parseIdToken(this.idToken);
        if (claims.get("user_id") != null)
            this.userId = (String) claims.get("user_id");
        Object temp = claims.get("exp");
        if (temp instanceof Long)
            this.expirationTime = (Long) temp;
        else if (temp instanceof Integer)
            this.expirationTime = ((Integer) temp).longValue();
        else this.expirationTime = 0;
    }


    public String getIdToken() {
        return idToken;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getUserId() {
        return userId;
    }

    public long getExpirationTime() {
        return expirationTime;
    }

    @NonNull
    @Override
    public String toString() {
        return "FirebaseRestAuthUser{" +
                "idToken='" + idToken + '\'' +
                ", email='" + email + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", userId='" + userId + '\'' +
                ", expirationTime=" + expirationTime +
                '}';
    }
}
