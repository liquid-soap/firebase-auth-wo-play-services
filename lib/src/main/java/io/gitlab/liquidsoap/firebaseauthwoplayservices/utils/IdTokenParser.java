/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.utils;

import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.util.Base64Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyMap;

public final class IdTokenParser {
    private IdTokenParser() {
        throw new IllegalStateException("Utility class");
    }

    public static Map<String, Object> parseIdToken(String idToken) {
        List<String> parts = Arrays.asList(idToken.split("\\."));
        if (parts.size() < 2) {
            return emptyMap();
        }

        String encodedToken = parts.get(1);
        try {
            String decodedToken = new String(Base64Utils.decodeUrlSafeNoPadding(encodedToken), Charset.defaultCharset());
            Map<String, Object> var14 = parseRawUserInfo(decodedToken);
            if (var14 == null) {
                var14 = emptyMap();
            }

            return var14;
        } catch (Exception e) {
            return emptyMap();
        }
    }


    public static Map<String, Object> toMap(JSONObject json) throws JSONException {
        Map<String, Object> map = new LinkedHashMap<>();
        Iterator<String> keyItr = json.keys();

        while (keyItr.hasNext()) {
            String key = keyItr.next();

            // Value can be a primitive, a map, or a list
            Object value = json.get(key);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }

            map.put(key, value);
        }

        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }

            list.add(value);
        }
        return list;
    }

    private static Map<String, Object> parseRawUserInfo(String rawUserInfo) {
        if (TextUtils.isEmpty(rawUserInfo)) {
            return null;
        }

        try {
            JSONObject jsonObject = new JSONObject(rawUserInfo);
            return jsonObject != JSONObject.NULL ? toMap(jsonObject) : null;
        } catch (Exception e) {
            Log.e("TAG", e.getMessage(), e);
            return null;
        }
    }
}
