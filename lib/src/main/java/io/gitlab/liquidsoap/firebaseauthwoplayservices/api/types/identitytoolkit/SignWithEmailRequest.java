/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit;

import androidx.annotation.NonNull;

/**
 * Reference: https://firebase.google.com/docs/reference/rest/auth#section-create-email-password
 * and https://firebase.google.com/docs/reference/rest/auth#section-sign-in-email-password
 */
public class SignWithEmailRequest {
    /**
     * The email the user is signing in/up with
     */
    public final String email;
    /**
     * The password for the account.
     */
    public final String password;
    /**
     * Whether or not to return an ID and refresh token. Should always be true.
     */
    public final Boolean returnSecureToken = true;

    public SignWithEmailRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @NonNull
    @Override
    public String toString() {
        return "SignWithEmailRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", returnSecureToken=" + returnSecureToken +
                '}';
    }
}
