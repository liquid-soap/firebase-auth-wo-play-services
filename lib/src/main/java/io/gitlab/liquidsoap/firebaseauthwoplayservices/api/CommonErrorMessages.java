/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api;

/**
 * List of common error messages
 */
public enum CommonErrorMessages {
    TOKEN_EXPIRED,
    USER_DISABLED,
    USER_NOT_FOUND,
    INVALID_REFRESH_TOKEN,
    INVALID_GRANT_TYPE,
    MISSING_REFRESH_TOKEN,
    EMAIL_EXISTS,
    OPERATION_NOT_ALLOWED,
    TOO_MANY_ATTEMPTS_TRY_LATER,
    EMAIL_NOT_FOUND,
    INVALID_PASSWORD,
    INVALID_IDP_RESPONSE,
    INVALID_CUSTOM_TOKEN,
    CREDENTIAL_MISMATCH
}
