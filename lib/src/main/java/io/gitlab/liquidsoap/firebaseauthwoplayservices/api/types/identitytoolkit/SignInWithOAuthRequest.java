/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit;

import androidx.annotation.NonNull;

/**
 * Reference:
 * https://firebase.google.com/docs/reference/rest/auth#section-sign-in-with-oauth-credential
 */
public class SignInWithOAuthRequest {
    /**
     * The URI to which the IDP redirects the user back.
     */
    public final String requestUri;
    /**
     * Contains the OAuth credential (an ID token or access token) and provider ID which issues
     * the credential.
     */
    public final String postBody;
    /**
     * Whether or not to return an ID and refresh token. Should always be true.
     */
    public final boolean returnSecureToken = true;
    /**
     * Whether to force the return of the OAuth credential on the following errors:
     * FEDERATED_USER_ID_ALREADY_LINKED and EMAIL_EXISTS.
     */
    public final boolean returnIdpCredential;

    public SignInWithOAuthRequest(String requestUri, String postBody, boolean returnIdpCredential) {
        this.requestUri = requestUri;
        this.postBody = postBody;
        this.returnIdpCredential = returnIdpCredential;
    }

    @NonNull
    @Override
    public String toString() {
        return "SignInWithOAuthRequest{" +
                "requestUri='" + requestUri + '\'' +
                ", postBody='" + postBody + '\'' +
                ", returnSecureToken=" + returnSecureToken +
                ", returnIdpCredential=" + returnIdpCredential +
                '}';
    }
}
