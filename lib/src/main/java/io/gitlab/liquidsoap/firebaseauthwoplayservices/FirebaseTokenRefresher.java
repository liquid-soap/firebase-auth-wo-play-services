/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices;

import android.os.Handler;
import android.util.Log;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;

import com.google.firebase.auth.internal.IdTokenListener;
import com.google.firebase.internal.InternalTokenResult;

import java.util.ArrayList;
import java.util.List;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.utils.ExpirationUtils;

public class FirebaseTokenRefresher implements IdTokenListener, LifecycleObserver {
    private static final String TAG = FirebaseTokenRefresher.class.getSimpleName();
    private static final long MIN_RETRY_BACKOFF_SECS = 30L;
    private static final long MAX_RETRY_BACKOFF_SECS = 300L;
    private final List<OnAuthStateChangeListener> listeners = new ArrayList<>();
    private final FirebaseRestAuth auth;
    private Long failureRetryTimeSecs;
    private String lastToken = null;
    private final Handler handler;
    private final Runnable refreshRunnable = new Runnable() {

        @Override
        public void run() {
            FirebaseRestAuthUser user = FirebaseTokenRefresher.this.getAuth().getCurrentUser();
            if (user == null) {
                Log.d(TAG, "User signed out, nothing to refresh.");
                return;
            }

            int minSecsRemaining = 600;
            long secsRemaining = ExpirationUtils.expiresInSeconds(user);
            long diffSecs = secsRemaining - (long) minSecsRemaining;

            // If the token has enough time left, run a refresh later.
            if (diffSecs > 0L) {
                Log.d(TAG, "Token expires in " + secsRemaining + ", scheduling refresh in " + diffSecs + " seconds");
                FirebaseTokenRefresher.this.handler.postDelayed(this, diffSecs * (long) 1000);
                return;
            }

            // Time to refresh the token now
            Log.d(TAG, "Token expires in " + secsRemaining + ", refreshing token now!");
            auth.getAccessToken(true)
                    .addOnSuccessListener(getTokenResult -> {
                        // On success just re-post, the logic above will handle the timing.
                        Log.d(TAG, "Token refreshed successfully." + this);
                        handler.post(this);

                        // Clear the failure backoff
                        failureRetryTimeSecs = MIN_RETRY_BACKOFF_SECS;
                    })
                    .addOnFailureListener(e -> {
                        Log.e(TAG, "Failed to refresh token", e);
                        Log.d(TAG, "Retrying in " + failureRetryTimeSecs + "...");

                        // Retry and double the backoff time (up to the max)
                        handler.postDelayed(this, failureRetryTimeSecs * 1000);
                        failureRetryTimeSecs = Math.min(failureRetryTimeSecs * 2, MAX_RETRY_BACKOFF_SECS);

                    });
        }
    };

    public FirebaseTokenRefresher(FirebaseRestAuth auth) {
        this.auth = auth;
        this.failureRetryTimeSecs = 30L;
        this.handler = new Handler();
    }

    public final FirebaseRestAuth getAuth() {
        return auth;
    }

    @Keep
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    void onLifecycleStarted() {
        this.start();
    }

    @Keep
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    void onLifecycleStopped() {
        this.stop();
    }

    public FirebaseTokenRefresher bindTo(LifecycleOwner owner) {
        owner.getLifecycle().addObserver(this);
        return this;
    }

    private void start() {
        Log.d(TAG, "start()");
        auth.addIdTokenListener(this);
        handler.post(refreshRunnable);
    }

    private void stop() {
        Log.d(TAG, "stop()");
        auth.removeIdTokenListener(this);
        handler.removeCallbacksAndMessages(null);
        lastToken = null;
    }

    @Override
    public void onIdTokenChanged(@NonNull InternalTokenResult res) {
        String token = res.getToken();
        if (token != null && lastToken == null) {
            // We are now signed in, time to start refreshing
            Log.d(TAG, "Token changed from null --> non-null, starting refreshing");
            handler.post(refreshRunnable);
        }

        if (lastToken != null && token == null) {
            // The user signed out, stop all refreshing
            Log.d(TAG, "Signed out, canceling refreshes");
            handler.removeCallbacksAndMessages(null);
        }
        for (OnAuthStateChangeListener listener : listeners) {
            listener.onStateChanged(auth.getCurrentUser());
        }
        lastToken = token;
    }

    /**
     * Listen to auth changes. Warning: immediately returns user state to listener object, so if you
     * attach it in your activity, you should do it after interface initialization.
     *
     * @param listener implements OnAuthStateChangeListener
     */
    public void addStateListener(OnAuthStateChangeListener listener) {
        this.listeners.add(listener);
        new Handler().post(() -> listener.onStateChanged(auth.getCurrentUser()));
    }

    public void removeStateListener(OnAuthStateChangeListener listener) {
        this.listeners.remove(listener);
    }
}
