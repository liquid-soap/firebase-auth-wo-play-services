/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.utils;

import java.util.Date;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuthUser;

public final class ExpirationUtils {
    private ExpirationUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static Boolean isExpired(FirebaseRestAuthUser user) {
        return expiresInSeconds(user) <= 0L;
    }

    public static Long expiresInSeconds(FirebaseRestAuthUser user) {
        long now = (new Date()).getTime() / (long) 1000;
        return user.getExpirationTime() - now;
    }
}
