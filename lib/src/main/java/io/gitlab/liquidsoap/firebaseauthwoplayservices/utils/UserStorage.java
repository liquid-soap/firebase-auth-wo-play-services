/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.utils;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.FirebaseApp;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuthUser;


public class UserStorage {
    private final SharedPreferences prefs;
    private final FirebaseApp app;

    public UserStorage(@NonNull Context context, @NonNull FirebaseApp app) {
        this.app = app;
        this.prefs = context.getSharedPreferences("UserStorage", Context.MODE_PRIVATE);
    }

    public final void set(@Nullable FirebaseRestAuthUser user) {
        if (user == null) {
            this.clear();
        } else
            this.prefs.edit()
                    .putString(getKey("idToken"), user.getIdToken())
                    .putString(getKey("email"), user.getEmail())
                    .putString(getKey("refreshToken"), user.getRefreshToken())
                    .putString(getKey("userId"), user.getUserId())
                    .apply();
    }

    @Nullable
    public final FirebaseRestAuthUser get() {
        String idToken = prefs.getString( getKey("idToken"), null);
        String refreshToken =  prefs.getString( getKey("refreshToken"), null);
        String email =  prefs.getString( getKey("email"), null);
        String localId = prefs.getString( getKey("userId"), null);
        return idToken != null && refreshToken != null ? new FirebaseRestAuthUser(idToken, email, refreshToken, localId) : null;
    }

    private void clear() {
        this.prefs.edit().clear().apply();
    }

    private String getKey(String field) {
        return "UserStorage___" + app.getOptions().getProjectId() + "__" + field;
    }
}
