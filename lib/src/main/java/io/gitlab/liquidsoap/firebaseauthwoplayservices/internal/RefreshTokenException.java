/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.internal;

import com.google.firebase.FirebaseException;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages;

public class RefreshTokenException extends FirebaseException {
    private int responseErrorCode = -1;
    private CommonErrorMessages responseErrorMessage = null;
    private String responseErrorStatus = null;

    public RefreshTokenException(CommonErrorMessages message) {
        super(message.name());
        this.responseErrorMessage = message;
    }

    public RefreshTokenException(CommonErrorMessages message, Throwable e) {
        super(message.name(), e);
    }

    public int getResponseErrorCode() {
        return responseErrorCode;
    }

    public RefreshTokenException setResponseErrorCode(int responseErrorCode) {
        this.responseErrorCode = responseErrorCode;
        return this;
    }

    public String getResponseErrorStatus() {
        return responseErrorStatus;
    }

    public RefreshTokenException setResponseErrorStatus(String responseErrorStatus) {
        this.responseErrorStatus = responseErrorStatus;
        return this;
    }

    public CommonErrorMessages getResponseErrorMessage() {
        return responseErrorMessage;
    }
}
