/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.utils;


import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuthException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.internal.RefreshTokenException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.INVALID_GRANT_TYPE;
import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.INVALID_REFRESH_TOKEN;
import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.MISSING_REFRESH_TOKEN;
import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.OPERATION_NOT_ALLOWED;
import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.TOKEN_EXPIRED;
import static io.gitlab.liquidsoap.firebaseauthwoplayservices.api.CommonErrorMessages.TOO_MANY_ATTEMPTS_TRY_LATER;

public final class RetrofitUtils {

    private RetrofitUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static <T> Task<T> callToTask(Call<T> call) {
        final TaskCompletionSource<T> source = new TaskCompletionSource<>();

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                T body = response.body();
                if (body == null) {
                    source.trySetException(parseException(response));
                } else {
                    source.trySetResult(body);
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                source.trySetException(new Exception(t));
            }
        });

        return source.getTask();
    }

    private static <T> Exception parseException(@NonNull Response<T> response) {
        ResponseBody errorBody = response.errorBody();
        if (errorBody == null) {
            return new Exception("Body null, status code " + response.code());
        }
        try {
            String errorNotParsed = errorBody.string();
            JSONObject jsonObject = new JSONObject(errorNotParsed).getJSONObject("error");
            if (jsonObject.has("message")) {
                String message = jsonObject.getString("message");
                try {
                    CommonErrorMessages m = CommonErrorMessages.valueOf(message);
                    if (TOKEN_EXPIRED.equals(m) || INVALID_REFRESH_TOKEN.equals(m) ||
                            MISSING_REFRESH_TOKEN.equals(m)) {
                        int code = jsonObject.getInt("code");
                        String status = jsonObject.getString("status");
                        return new RefreshTokenException(m)
                                .setResponseErrorCode(code)
                                .setResponseErrorStatus(status);
                    }
                    if (TOO_MANY_ATTEMPTS_TRY_LATER.equals(m) || OPERATION_NOT_ALLOWED.equals(m) ||
                            INVALID_GRANT_TYPE.equals(m)) {
                        return new FirebaseApiNotAvailableException(message);
                    }
                    return new FirebaseAuthException("Response body == null", message);
                } catch (IllegalArgumentException | JSONException e) {
                    return new FirebaseException(message);
                }
            }

        } catch (IOException | JSONException | NullPointerException ignored) {
            // suppress in case this wasn't firebase error. Could be network exception
        }
        return new Exception("Body null, status code " + response.code());
    }
}
