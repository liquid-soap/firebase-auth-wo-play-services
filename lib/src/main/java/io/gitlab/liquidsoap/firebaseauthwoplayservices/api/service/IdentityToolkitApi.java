/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.service;

import androidx.annotation.NonNull;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.FetchProvidersForEmailRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.FetchProvidersForEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.OOBEmailRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.OOBEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInAnonymouslyRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInAnonymouslyResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithCustomTokenRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithCustomTokenResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithOAuthRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignInWithOAuthResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignUpWithEmailResponse;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.SignWithEmailRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.VerifyOOBRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit.VerifyOOBResponse;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Implementation of IdentityToolkit API
 * See also https://firebase.google.com/docs/reference/rest/auth
 */
public interface IdentityToolkitApi {

    @POST("v1/accounts:signUp")
    Call<SignInAnonymouslyResponse> signInAnonymously(@Body @NonNull SignInAnonymouslyRequest request);

    @POST("v1/accounts:signInWithCustomToken")
    Call<SignInWithCustomTokenResponse> signInWithCustomToken(@Body @NonNull SignInWithCustomTokenRequest request);

    @POST("v1/accounts:signInWithPassword")
    Call<SignInWithEmailResponse> signInWithPassword(@Body SignWithEmailRequest request);

    @POST("v1/accounts:signUp")
    Call<SignUpWithEmailResponse> signUpWithEmail(@Body SignWithEmailRequest request);

    @POST("v1/accounts:signInWithIdp")
    Call<SignInWithOAuthResponse> signInWithOAuth(@Body SignInWithOAuthRequest request);

    @POST("v1/accounts:createAuthUri")
    Call<FetchProvidersForEmailResponse>
    fetchEmailProviders(@Body FetchProvidersForEmailRequest request);

    @POST("v1/accounts:sendOobCode")
    Call<OOBEmailResponse> sendOOBEmailCode(@Body OOBEmailRequest request);

    @POST("v1/accounts:sendOobCode")
    Call<VerifyOOBResponse> verifyOOBEmailCode(@Body VerifyOOBRequest request);

    static IdentityToolkitApi getInstance(OkHttpClient client) {
        String baseUrl = "https://identitytoolkit.googleapis.com/";
        // Retrofit client pointed at the Firebase IdentityToolkit API
        Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).client(client)
                .addConverterFactory(GsonConverterFactory.create()).build();

        return retrofit.create(IdentityToolkitApi.class);
    }
}
