/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.internal;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.internal.InternalAuthProvider;
import com.google.firebase.components.Component;
import com.google.firebase.components.ComponentRegistrar;
import com.google.firebase.components.Dependency;

import java.util.ArrayList;
import java.util.List;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.FirebaseRestAuth;


/**
 * Required so other Firebase libraries can find this implementation of InternalAuthProvider.
 * Note, you cannot also include the FirebaseAuth client library in your build.
 */
public final class RestAuthRegistrar implements ComponentRegistrar {
    @Override
    public List<Component<?>> getComponents() {
        Component<InternalAuthProvider> restAuthComponent = Component.builder(InternalAuthProvider.class)
                .add(Dependency.required(FirebaseApp.class))
                .factory(componentContainer -> {
                    FirebaseApp firebaseApp = componentContainer.get(FirebaseApp.class);

                    return FirebaseRestAuth.getInstance(firebaseApp);
                }).build();
        List<Component<?>> list = new ArrayList<>();
        list.add(restAuthComponent);
        return list;
    }
}
