/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit;

import androidx.annotation.NonNull;

/**
 * Reference: https://firebase.google.com/docs/reference/rest/auth#section-verify-custom-token
 */
public class SignInWithCustomTokenResponse {
    /**
     * A Firebase Auth ID token for the newly created user
     */
    public String idToken = "";
    /**
     * A Firebase Auth refresh token for the newly created user, generated from
     * the provided custom token.
     */
    public String refreshToken = "";
    /**
     * The number of seconds in which the ID token expires
     */
    public String expiresIn = "";

    @NonNull
    @Override
    public String toString() {
        return "SignInWithCustomTokenResponse{" +
                "idToken='" + idToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", expiresIn='" + expiresIn + '\'' +
                '}';
    }
}
