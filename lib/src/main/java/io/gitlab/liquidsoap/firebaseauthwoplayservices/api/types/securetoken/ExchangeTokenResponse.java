/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.securetoken;

import androidx.annotation.NonNull;

/**
 * Reference: https://firebase.google.com/docs/reference/rest/auth#section-refresh-token
 */
public class ExchangeTokenResponse {
    /**
     * The number of seconds in which the ID token expires.
     */
    public String expires_in;
    /**
     * The type of the refresh token, always "Bearer".
     */
    public String token_type;
    /**
     * The Firebase Auth refresh token provided in the request or a new refresh token.
     */
    public String refresh_token;
    /**
     * A Firebase Auth ID token.
     */
    public String id_token;
    /**
     * The uid corresponding to the provided ID token.
     */
    public String user_id;
    /**
     * Your Firebase project ID.
     */
    public String project_id;


    @NonNull
    @Override
    public String toString() {
        return "ExchangeTokenResponse{" +
                "expires_in='" + expires_in + '\'' +
                ", token_type='" + token_type + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", id_token='" + id_token + '\'' +
                ", user_id='" + user_id + '\'' +
                ", project_id='" + project_id + '\'' +
                '}';
    }
}
