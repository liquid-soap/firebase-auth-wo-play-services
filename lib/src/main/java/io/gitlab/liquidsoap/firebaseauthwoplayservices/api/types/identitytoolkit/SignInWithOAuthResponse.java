/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.identitytoolkit;


import androidx.annotation.NonNull;

/**
 * Reference:
 * https://firebase.google.com/docs/reference/rest/auth#section-sign-in-with-oauth-credential
 */
public class SignInWithOAuthResponse {
    /**
     * The unique ID identifies the IdP account.
     */
    public String federatedId;
    /**
     * The linked provider ID (e.g. "google.com" for the Google provider).
     */
    public String providerId;
    /**
     * The uid of the authenticated user.
     */
    public String localId;
    /**
     * Whether the sign-in email is verified.
     */
    public boolean emailVerified;
    /**
     * The email of the account.
     */
    public String email;
    /**
     * The OIDC id token if available.
     */
    public String oauthIdToken;
    /**
     * The OAuth access token if available.
     */
    public String oauthAccessToken;
    /**
     * The OAuth 1.0 token secret if available.
     */
    public String oauthTokenSecret;
    /**
     * The stringified JSON response containing all the IdP data corresponding to the provided
     * OAuth credential.
     */
    public String rawUserInfo;
    /**
     * The first name for the account.
     */
    public String firstName;
    /**
     * The last name for the account.
     */
    public String lastName;
    /**
     * The full name for the account.
     */
    public String fullName;
    /**
     * The display name for the account.
     */
    public String displayName;
    /**
     * The photo Url for the account.
     */
    public String photoUrl;
    /**
     * A Firebase Auth ID token for the authenticated user.
     */
    public String idToken;
    /**
     * A Firebase Auth refresh token for the authenticated user.
     */
    public String refreshToken;
    /**
     * The number of seconds in which the ID token expires.
     */
    public String expiresIn;
    /**
     * Whether another account with the same credential already exists. The user will need to sign
     * in to the original account and then link the current credential to it.
     */
    public boolean needConfirmation;

    @NonNull
    @Override
    public String toString() {
        return "SignInWithOAuthResponse{" +
                "federatedId='" + federatedId + '\'' +
                ", providerId='" + providerId + '\'' +
                ", localId='" + localId + '\'' +
                ", emailVerified=" + emailVerified +
                ", email='" + email + '\'' +
                ", oauthIdToken='" + oauthIdToken + '\'' +
                ", oauthAccessToken='" + oauthAccessToken + '\'' +
                ", oauthTokenSecret='" + oauthTokenSecret + '\'' +
                ", rawUserInfo='" + rawUserInfo + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", displayName='" + displayName + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", idToken='" + idToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", expiresIn='" + expiresIn + '\'' +
                ", needConfirmation=" + needConfirmation +
                '}';
    }
}
