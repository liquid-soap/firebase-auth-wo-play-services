/*
 * Copyright 2020 Liquid Soap <liquid.soap.dev@gmail.com>. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.gitlab.liquidsoap.firebaseauthwoplayservices.api.service;

import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.securetoken.ExchangeTokenRequest;
import io.gitlab.liquidsoap.firebaseauthwoplayservices.api.types.securetoken.ExchangeTokenResponse;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SecureTokenApi {
    @POST("v1/token")
    Call<ExchangeTokenResponse> exchangeToken(@Body ExchangeTokenRequest request);

    static SecureTokenApi getInstance(OkHttpClient client) {
        String baseUrl = "https://securetoken.googleapis.com/";
        // Retrofit client pointed at the Firebase Auth API
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(SecureTokenApi.class);
    }
}
